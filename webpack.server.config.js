const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const cssProd = ExtractTextPlugin.extract({
  use: ['css-loader', 'postcss-loader', 'sass-loader'],
  fallback: 'style-loader',
});

module.exports = {
  entry: path.join(__dirname, 'src', 'ssr.js'),
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'server.js',
    libraryTarget: 'commonjs2',
    publicPath: '/dist/',
  },
  module: {
    loaders: [
      {
        test: /\.s?css$/,
        use: cssProd,
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
      },
    ],
  },
  plugins: [
    new ExtractTextPlugin({
      filename: 'css/[name].css',
      allChunks: true,
    }),
  ],
};
