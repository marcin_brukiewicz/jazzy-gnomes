const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const cssDev = ['style-loader', 'css-loader', 'postcss-loader', 'sass-loader'];

module.exports = {
  entry: {
    vendor: ['react', 'react-dom'],
    app: path.join(__dirname, 'src', 'app.js'),
  },
  output: {
    path: path.join(__dirname, 'dist'),
    filename: '[name].bundle.js',
  },
  module: {
    loaders: [
      {
        test: /\.s?css$/,
        use: cssDev,
      },
      {
        test: /\.js$/,
        loader: 'babel-loader', // Options are defined in .babelrc
        exclude: /(node_modules|bower_components)/,
      },
      {
        test: /\.(png|svg|jpe?g|gif)$/i,
        loaders: [
          'file-loader?hash=sha512&digest=hex&name=[hash].[ext]',
          'image-webpack-loader?bypassOnDebug&optimizationLevel=7&interlaced=false',
        ],
      },
      { test: /\.(woff2?|svg)$/, loader: 'url-loader?limit=10000&name=fonts/[name].[ext]' },
      { test: /\.(ttf|eot)$/, loader: 'file-loader?name=fonts/[name].[ext]' },
    ],
  },
  devtool: 'source-map',
  devServer: {
    contentBase: [path.join(__dirname, 'dist'), path.join(__dirname, 'public')],
    compress: true,
    hot: true,
    historyApiFallback: true,
    stats: 'errors-only',
    port: 3137,
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
    }),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery',
    }),
    new HtmlWebpackPlugin({
      title: 'The project',
      minify: {
        collapseWhitespace: false,
      },
      hash: true,
      chunks: ['vendor', 'app'],
      filename: 'index.html',
      template: './src/templates/index.ejs',
    }),
    new ExtractTextPlugin({
      filename: 'css/[name].css',
      disable: true,
      allChunks: true,
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(),
  ],
};
