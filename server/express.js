const express = require('express');
const path = require('path');
const ServerRenderer = require('../dist/server.js').default;


const app = express();

// App options
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, '..', '/dist'));
app.set('port', (process.env.PORT || 3000));

// Middlewares
app.use(express.static(path.join(__dirname, '..', '/dist')));
app.use(express.static(path.join(__dirname, '..', '/public')));

app.use(ServerRenderer());

app.listen(app.get('port'), function () {
  console.log('Listening on port', app.get('port'));
});
