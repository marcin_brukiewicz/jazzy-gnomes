# Jazzy Gnomes

Simple app for Jazzy


### Installing

```
git clone git@bitbucket.org:marcin_brukiewicz/jazzy-gnomes.git folder-name
yarn run build // Will download dependencies and rebuild the dist folder
yarn run dev   // To start webpack-dev-server or
yarn run start // To start production server
```


## License

This project is licensed under the MIT License.
