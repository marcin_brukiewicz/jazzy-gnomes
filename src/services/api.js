import axios from 'axios';

const create = () => {
  const baseURL = 'http://master.datasource.jazzy-hr.jzapp.io/api/v1';
  const api = axios.create({
    baseURL,
  });

  const getGnomes = (page, limit) => api.get('gnomes', { params: { limit, offset: (page - 1) * limit } });
  const updateGnome = gnome => api.patch(`gnomes/${gnome.id}`, {
    params: { name: gnome.name, age: gnome.age },
  });
  return {
    getGnomes,
    updateGnome,
  };
};

export default {
  create,
};
