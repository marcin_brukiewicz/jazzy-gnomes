export default ({ markup, helmet }) => {
	return `<!doctype html>
<html ${helmet.htmlAttributes.toString()}>
<head>
	${helmet.title.toString()}
	${helmet.meta.toString()}
	${helmet.link.toString()}
	<link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,800" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link rel="icon" type="image/png" href="/favico.png" />
</head>
<body ${helmet.bodyAttributes.toString()}>
	<div id="root">${markup}</div>
	<script src="/vendor.bundle.js" async></script>
	<script src="/app.bundle.js" async></script>
</body>
</html>`;
};
