import * as types from './types';

export const changePage = page => dispatch =>
  dispatch({
    type: types.CHANGE_PAGE,
    payload: page,
  });

export const loadGnomes = (page, limit = 50) => (dispatch, getState, { api }) =>
  dispatch({
    type: types.LOAD_GNOMES,
    promise: api.getGnomes(page, limit),
    meta: {
      page,
    },
  });

export const updateGnome = gnome => (dispatch, getState, { api }) =>
  dispatch({
    type: types.UPDATE_GNOME,
    promise: api.updateGnome(gnome),
  });

export const showUpdateModal = gnome => dispatch =>
  dispatch({
    type: types.SHOW_UPDATE_MODAL,
    payload: {
      visible: true,
      gnome,
    },
  });

export const hideUpdateModal = () => dispatch =>
  dispatch({
    type: types.HIDE_UPDATE_MODAL,
    payload: {
      visible: false,
      gnome: null,
    },
  });

export default {
  loadGnomes,
  updateGnome,
  showUpdateModal,
  hideUpdateModal,
};
