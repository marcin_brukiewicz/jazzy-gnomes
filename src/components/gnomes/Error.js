import React from 'react';

export default () => (
  <div>
    <h5>There was error fetching our data :(</h5>
    <button className="btn">Try again</button>
  </div>
);
