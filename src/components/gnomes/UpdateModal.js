import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Form, Input, Button, message } from 'antd';

class UpdateModal extends React.Component {
  handleCancel = () => {
    this.props.hideUpdateModal();
  }
  handleSubmit = (e) => {
    e.preventDefault();
    const { updateGnome, form, modal } = this.props;
    this.setState({ confirmLoading: true });
    updateGnome({ ...form.getFieldsValue(), id: modal.gnome.id })
      .then((response) => {
        if (response.payload.status === 200) {
          this.props.hideUpdateModal();
          message.success('Gnome updated!');
        }
      });
  }

  render() {
    const { modal: { gnome, loading, error }, form: { getFieldDecorator } } = this.props;
    return (
      <Modal
        title="Update"
        visible
        onCancel={this.handleCancel}
        footer={null}
      >
        <Form
          layout="vertical"
          onSubmit={this.handleSubmit}
          className="update-gnome"
          hideRequiredMark
        >
          <Form.Item label="Name">
            {getFieldDecorator('name', {
              rules: [{ required: true, message: 'Please input gnome name!' }],
              initialValue: gnome.name,
            })(
              <Input placeholder="Username" />,
            )}
          </Form.Item>
          <Form.Item label="Age">
            {getFieldDecorator('age', {
              rules: [{ required: true, message: 'Please input gnome age!' }],
              initialValue: gnome.age,
            })(
              <Input placeholder="Age" />,
            )}
          </Form.Item>
          <Button htmlType="submit" className="update-gnome-button purple" loading={loading}>
            Update
          </Button>
          {error && <span style={{ color: 'red', marginLeft: '8px' }}>Oops! Something went wrong!</span>}
        </Form>
      </Modal>
    );
  }
}

UpdateModal.propTypes = {
  updateGnome: PropTypes.func.isRequired,
  modal: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  form: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  hideUpdateModal: PropTypes.func.isRequired,
};

const WrappedModal = Form.create()(UpdateModal);

export default WrappedModal;
