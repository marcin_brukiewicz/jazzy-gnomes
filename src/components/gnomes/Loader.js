import React from 'react';
import { Wave } from 'better-react-spinkit';

const Loader = () => (
  <Wave className="loader" size={50} color="#9d0060" />
);

export default Loader;
