import React from 'react';
import PropTypes from 'prop-types';
import ReactPagination from 'react-paginate';

const Pagination = ({ currentPage, limit, onPageChange }) => (
  <ReactPagination
    containerClassName="pagination center-align"
    activeClassName="active"
    initialPage={currentPage}
    pageCount={5000 / limit}
    pageRangeDisplayed={5}
    marginPagesDisplayed={2}
    onPageChange={onPageChange}
  />
);

Pagination.propTypes = {
  currentPage: PropTypes.number.isRequired,
  limit: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired,
};

export default Pagination;
