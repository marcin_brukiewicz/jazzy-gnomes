import React from 'react';
import PropTypes from 'prop-types';
import { Table } from 'antd';

const InfoColumn = ({ name, age }) => (
  <div className="gnome--info flex__row">
    <img src="/gnome-av.png" alt="Gnome avatar" />
    <div className="flex__column cen-ver">
      <span className="name bold">{name}</span>
      <span className="age light">Age: {age}</span>
    </div>
  </div>
);

InfoColumn.propTypes = {
  name: PropTypes.string.isRequired,
  age: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]).isRequired,
};

const StrengthColumn = ({ strength }) => {
  const barWidth = 160;
  return (
    <div className="gnome--strength flex__row cen-ver">
      <div className="bar" style={{ width: barWidth }}>
        <div className="bar--fill" style={{ width: (barWidth / 100) * strength }} />
      </div>
      <span className="strength--value">{strength}/100</span>
      <span className="strength">Strength</span>
    </div>
  );
};

StrengthColumn.propTypes = {
  strength: PropTypes.number.isRequired,
};

const columns = [
  {
    title: 'info',
    key: 'info',
    index: 'info',
    render: (text, record) => <InfoColumn {...record} />,
  },
  {
    title: 'strength',
    key: 'strength',
    index: 'strength',
    render: (text, record) => <StrengthColumn strength={record.strenght} />,
  },
];

const List = ({ list, currentPage, showUpdateModal }) => {
  const data = list[currentPage].map(gnome => ({ ...gnome, key: gnome.id }));
  return (
    <div className="list">
      <Table
        columns={columns}
        dataSource={data}
        pagination={false}
        onRowClick={showUpdateModal}
      />
    </div>
  );
};

List.propTypes = {
  list: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
  currentPage: PropTypes.number.isRequired,
  showUpdateModal: PropTypes.func.isRequired,
};

export default List;
