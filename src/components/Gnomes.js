import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import queryString from 'query-string';
import { Pagination } from 'antd';
import Helmet from 'react-helmet';
import UpdateModal from './gnomes/UpdateModal';
import Error from './gnomes/Error';
import Loader from './gnomes/Loader';
import List from './gnomes/List';
import * as actions from '../actions';

class Gnomes extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      limit: 50,
      modalVisible: false,
    };
  }
  componentWillMount() {
    const page = parseInt(queryString.parse(this.props.history.location.search).page, 10) || 1;
    this.props.changePage(page);
    const { gnomes: { list }, loadGnomes } = this.props;
    if (!list[page]) {
      loadGnomes(page);
    }
  }
  onPageChange = (selected) => {
    const { gnomes: { list, currentPage }, loadGnomes, changePage, history } = this.props;
    if (currentPage === selected) { return; }
    changePage(selected);
    if (!list[selected]) {
      loadGnomes(selected);
    }
    history.push(`/gnomes?page=${selected}`);
  }
  showModal = () => {
    this.setState({ modalVisible: true });
  }
  render() {
    const {
      gnomes: { error, list, loading, currentPage },
      modals,
      showUpdateModal,
      hideUpdateModal,
      updateGnome,
    } = this.props;
    const { limit } = this.state;
    const totalResults = 5000; // Hard-coded as no helpful endpoint is provided.
    const result = loading || Object.keys(list).length === 0 || !list[currentPage] ?
      <Loader /> :
      (<List
        currentPage={currentPage}
        list={list}
        pageChange={this.onPageChange}
        limit={limit}
        showUpdateModal={showUpdateModal}
      />);
    if (error) {
      return <Error />;
    }
    return (
      <div className="row">
        <Helmet title="Gnomes" />
        <div className="col s12">
          <div className="gnomes flex__column">
            {modals.update.visible &&
              <UpdateModal
                modal={modals.update}
                hideUpdateModal={hideUpdateModal}
                updateGnome={updateGnome}
              />}
            <h1>Gnomes</h1>
            {result}
            <Pagination
              current={currentPage}
              onChange={this.onPageChange}
              pageSize={limit}
              pageDefaultSize={limit}
              total={totalResults}
              className="pagination"
            />
          </div>
        </div>
      </div>
    );
  }
}

Gnomes.propTypes = {
  gnomes: PropTypes.shape({
    list: PropTypes.object.isRequired,
    loading: PropTypes.bool.isRequired,
    error: PropTypes.string,
    currentPage: PropTypes.number.isRequired,
  }).isRequired,
  modals: PropTypes.shape({
    update: PropTypes.shape({
      visible: PropTypes.bool.isRequired,
      gnome: PropTypes.object,
    }),
  }).isRequired,
  changePage: PropTypes.func.isRequired,
  loadGnomes: PropTypes.func.isRequired,
  updateGnome: PropTypes.func.isRequired,
  showUpdateModal: PropTypes.func.isRequired,
  hideUpdateModal: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired, // eslint-disable-line react/forbid-prop-types
};

const mapStateToProps = state => ({
  gnomes: state.gnomes,
  modals: state.modals,
});


export default connect(mapStateToProps, actions)(Gnomes);
