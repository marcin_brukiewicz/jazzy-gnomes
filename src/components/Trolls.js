import React from 'react';
import Helmet from 'react-helmet';

const Trolls = () => (
  <div className="full-height flex__column cen-ver cen-hor">
    <Helmet title="Trolls" />
    <h1 className="text-center">Sorry! Trolls are under development!<br />They should be rising soon!</h1>
  </div>
);

export default Trolls;
