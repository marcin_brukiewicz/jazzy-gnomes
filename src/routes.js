import React from 'react';
import { Route } from 'react-router-dom';
import Helmet from 'react-helmet';
import Layout from './layouts';
import Gnomes from './components/Gnomes';
import Trolls from './components/Trolls';


export default () => (
  <Layout>
    <Helmet
      htmlAttributes={{ lang: 'en', amp: undefined }} // amp takes no value
      titleTemplate="%s | Jazzy Gnomes!"
      titleAttributes={{ itemprop: 'name', lang: 'en' }}
      meta={[
        { name: 'description', content: 'Jazzy Gnomes!' },
        { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      ]}
    />
    <Route
      exact
      path="/"
      render={() => (
        <div className="full-height flex__column cen-ver cen-hor">
          <Helmet title="Home" />
          <h1 className="text-center">Welcome to the creature&apos;s world!<br />Choose your unit above!</h1>
        </div>
      )}
    />
    <Route path="/gnomes" component={Gnomes} />
    <Route path="/trolls" component={Trolls} />
  </Layout>
);
