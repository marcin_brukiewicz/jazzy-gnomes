import { combineReducers } from 'redux';
import { handle } from 'redux-pack';
import { SHOW_UPDATE_MODAL, HIDE_UPDATE_MODAL, UPDATE_GNOME } from '../actions/types';

const initialState = {
  visible: false,
  gnome: null,
  loading: false,
  error: null,
};

const update = (state = initialState, action) => {
  const { payload, type } = action;
  switch (type) {
    case SHOW_UPDATE_MODAL:
    case HIDE_UPDATE_MODAL:
      return {
        ...state,
        visible: payload.visible,
        gnome: payload.gnome,
      };
    case UPDATE_GNOME:
      return handle(state, action, {
        start: prevState => ({
          ...prevState,
          loading: true,
          error: null,
        }),
        finish: prevState => ({ ...prevState, loading: false }),
        failure: prevState => ({ ...prevState, error: payload.message }),
      });
    default:
      return state;
  }
};

export default combineReducers({
  update,
});
