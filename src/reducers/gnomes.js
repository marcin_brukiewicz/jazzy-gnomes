import { handle } from 'redux-pack';
import { LOAD_GNOMES, UPDATE_GNOME, CHANGE_PAGE } from '../actions/types';

const initialState = {
  list: {},
  error: null,
  loading: false,
  limit: 0,
  currentPage: 1,
};

// REMEMBER TO MERGE BRANCHES OR SOMETHING

export default (state = initialState, action) => {
  const { payload, meta } = action;
  switch (action.type) {
    case CHANGE_PAGE:
      return { ...state, currentPage: action.payload };
    case LOAD_GNOMES:
      return handle(state, action, {
        start: prevState => ({
          ...prevState,
          loading: true,
          error: null,
        }),
        finish: prevState => ({ ...prevState, loading: false }),
        failure: prevState => ({ ...prevState, error: payload.message }),
        success: (prevState) => {
          const list = { ...prevState.list };
          list[meta.page] = payload.data;
          return { ...prevState, list };
        },
      });
    case UPDATE_GNOME:
      return handle(state, action, {
        success: (prevState) => {
          const list = { ...prevState.list };
          list[state.currentPage] = list[state.currentPage].map((gnome) => {
            if (gnome.id == payload.data.id) {
              return {
                ...gnome,
                name: payload.data.params.name,
                age: payload.data.params.age,
              };
            }
            return gnome;
          });
          return {
            ...prevState,
            list,
          };
        },
      });
    default:
      return state;
  }
};
