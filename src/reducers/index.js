import { combineReducers } from 'redux';
import { reducer as form } from 'redux-form';
import gnomes from './gnomes';
import modals from './modals';

export default combineReducers({
  gnomes,
  modals,
  // form,
});
