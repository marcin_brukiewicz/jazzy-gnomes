import React from 'react';
import ReactDOMServer from 'react-dom/server';
import { StaticRouter } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import { Provider } from 'react-redux';
import Template from './template';
import Routes from './routes';
import createStore from './createStore';

require('./styles/main.scss');

const serverRenderer = () => (req, res) => {
  const context = {};
  const store = createStore();
  const markup = ReactDOMServer.renderToString(
    <Provider store={store}>
      <StaticRouter location={req.url} context={context}>
        <Routes />
      </StaticRouter>
    </Provider>,
  );
  const helmet = Helmet.renderStatic();
  res.status(200).send(Template({
    markup,
    helmet
  }));
};

export default serverRenderer;
