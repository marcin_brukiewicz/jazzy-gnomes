import React from 'react';
import PropTypes from 'prop-types';
import { Layout } from 'antd';
import Navbar from './Navbar';

const { Header, Content } = Layout;

const MyLayout = props => (
  <Layout>
    <Header className="navbar">
      <Navbar />
    </Header>
    <Content>
      <div className="container">
        {props.children}
      </div>
    </Content>
  </Layout>
);

MyLayout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default MyLayout;
