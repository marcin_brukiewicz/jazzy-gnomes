import React from 'react';
import { Button, Menu } from 'antd';
import { Link } from 'react-router-dom';

const Navbar = () => (
  <div className="container">
    <div className="nav--logo">
      <Link to="/">
        <img
          src="/logo.png"
          srcSet="/logo.png 1x, /logo@2x.png 2x, /logo@3x.png 3x"
          alt="logo"
        />
      </Link>
    </div>
    <Menu
      mode="horizontal"
      style={{ lineHeight: '64px' }}
    >
      <Menu.Item key="1">
        <Link to="/gnomes">Gnomes</Link>
      </Menu.Item>
      <Menu.Item key="2">
        <Link to="/trolls">Trolls</Link>
      </Menu.Item>
    </Menu>
    <div className="right options flex__row cen-ver">
      <Button className="create-monster purple">Create monster</Button>
      <div className="user flex__row cen-ver">
        <img src="/avatar.png" srcSet="/avatar.png 1x, /avatar@2x.png 2x, /avatar@3x.png 3x" alt="User avatar" />
        <div className="user--info flex__column cen-ver">
          <span className="bold">Robert Łabuś</span>
          <span className="light">Game Master</span>
        </div>
      </div>
    </div>
  </div>
);

export default Navbar;
